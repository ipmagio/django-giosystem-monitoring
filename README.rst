.. note::

   **This project is now deprecated**

   The code has been split into two different projects, 
   ``giosystem-monitor-ops`` and ``giosystem-monitor-quality``. These can be
   found on gitlab.com at:

   * https://gitlab.com/giosystem/giosystem-monitor-ops
   * https://gitlab.com/giosystem/giosystem-monitor-quality


This is a django app to store monitoring information for giosystem.

Installation
============

Create a virtualenv and activate it

.. code:: bash

   mkdir -p ~/dev/
   virtualenv venv
