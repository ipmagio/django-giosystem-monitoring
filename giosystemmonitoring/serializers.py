from rest_framework import serializers

from giosystemmonitoring.models import Operation, ProductQualityMonitor, QualityParameter

from rest_framework.response import Response


class OperationSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Operation
        fields = ('id', 'ecflow_host', 'ecflow_path', 'timestamp_begin',
                  'timestamp_end', 'timeslot', 'status', 'result',
                  'observation', 'host', 'settings_uri', 'package',
                  'number_of_executions', 'execution_time', 'suite', 'task')

class QualityParameterSerializer(serializers.ModelSerializer):

    class Meta:
        model = QualityParameter
        fields = ("id","name", "value")


class ProductQualityMonitorSerializer(serializers.ModelSerializer):

    parameters = QualityParameterSerializer(many=True)
    
    class Meta:
        model = ProductQualityMonitor
        fields = ("id","product_name", "area", "timeslot", "processing_line",
                  "updated_on", 'parameters')

    def create(self, validated_data):
        params_data = validated_data.pop('parameters')
        product = ProductQualityMonitor.objects.create(**validated_data)
        for param_data in params_data:
            QualityParameter.objects.create(product=product, **param_data)
        return product

     #def update(self, instance, validated_data):
        #params_data = validated_data.pop('parameters')
        #for param in params_data:
            #param, created = QualityParameter.objects.update_or_create(product=instance)
            #instance.parameters.add(param)
        #return instance
