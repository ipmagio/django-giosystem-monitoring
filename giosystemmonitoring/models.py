from django.db import models

class Operation(models.Model):
    WAITING = 'WAITING'
    RUNNING = 'RUNNING'
    FINISHED = 'FINISHED'
    STATUS_CHOICES = (
        (WAITING, WAITING),
        (RUNNING, RUNNING),
        (FINISHED, FINISHED),
    )
    ecflow_host = models.CharField(max_length=50, help_text='Name of the '
                                   'ecFlow server that ran this operation')
    ecflow_path = models.CharField(max_length=255, help_text='ecFlow path '
                                   'to the task that ran this operation')
    suite = models.CharField(max_length=255, help_text="ecFlow suite name")
    task = models.CharField(max_length=255, help_text="ecFlow task name")
    timestamp_begin = models.DateTimeField(help_text='Creation date for this '
                                           'operation')
    timestamp_end = models.DateTimeField(blank=True, null=True,
                                         help_text='End date for this '
                                         'operation')
    timeslot = models.DateTimeField(help_text='Timeslot for the package that '
                                    'the operation ran')
    status = models.CharField(max_length=20, choices=STATUS_CHOICES,
                              default=RUNNING)
    result = models.BooleanField(default=False)
    observation = models.TextField(help_text="Additional details about the "
                                             "operation's result")
    host = models.CharField(max_length=50, help_text='Name of the host where '
                            'this operation ran')
    settings_uri = models.CharField(max_length=255, help_text='URI of the '
                                    'settings that were used when configuring '
                                    'the package')
    package = models.CharField(max_length=50, help_text='Name of the package '
                               'that this operation ran')
    number_of_executions = models.SmallIntegerField(help_text='How many times '
                                                    'has this task been run. ',
                                                    default=0)

    def save(self, *args, **kwargs):
        if self.status == self.RUNNING: # record is being run
            self.number_of_executions += 1
        super(Operation, self).save(*args, **kwargs)

    def execution_time(self):
        if self.timestamp_end is None:
            result = 'Still running...'
        else:
            delta = self.timestamp_end - self.timestamp_begin
            days = delta.days
            hours = delta.seconds / (60 * 60)
            remaining_seconds = delta.seconds - hours * 60 * 60
            minutes = remaining_seconds / 60
            remaining_seconds = remaining_seconds - minutes * 60
            if delta.days > 0:
                result = '%id' % delta.days
            else:
                result = ''
            result = '%ih %im %is' % (hours, minutes, remaining_seconds)
        return result


class ProductQualityMonitor(models.Model):
    EURO = "Euro"
    AFRI = "Afri"
    ASIA = "Asia"
    NOAM = "Noam"
    SOAM = "Soam"
    OCEA = "Ocea"
    GLOBE = "Globe"
    AREA_CHOICES = (
        (GLOBE, GLOBE),
        (EURO, EURO),
        (AFRI, AFRI),
        (ASIA, ASIA),
        (NOAM, NOAM),
        (SOAM, SOAM),
        (OCEA, OCEA),
    )
    LST = "LST"
    LST10 = "LST10"
    PRODUCT_CHOICES = (
        (LST, LST),
        (LST10, LST10),
    )
    product_name = models.CharField(max_length=50, choices=PRODUCT_CHOICES,
                                    help_text="Name of the product that is "
                                              "being monitored")
    area = models.CharField(max_length=10, choices=AREA_CHOICES, default=GLOBE)
    timeslot = models.DateTimeField(help_text="Product timeslot")
    processing_line = models.CharField(max_length=50, blank=True)
    updated_on = models.DateTimeField(auto_now=True)

    def get_measured_parameters(self):
        params = []
        for p in self.parameters.all():
            params.append((p.name, p.value))
        return "<br>".join(["{0} = {1}".format(*p) for p in params])
    get_measured_parameters.allow_tags = True
    get_measured_parameters.short_description = "measured_parameters"

    def __unicode__(self):
        return "{} - {} - {}".format(self.product_name, self.area,
                                     self.timeslot)


class QualityParameter(models.Model):
    MEAN = "mean"
    MEDIAN = "median"
    MAXIMUM = "maximum"
    MINIMUM = "minimum"
    TIMELINESS = "timeliness"
    FIRST_QUARTILE = "first quartile"
    THIRD_QUARTILE = "third quartile"
    PERCENTILE_05 = "percentile 5"
    PERCENTILE_95 = "percentile 95"
    STANDARD_DEVIATION = "standard deviation"
    FILE_SIZE = "file size"
    PROCESSED_PIXELS = "processed pixels"
    POSITIVE_OUTLIERS = "positive outliers"
    NEGATIVE_OUTLIERS = "negative outliers"
    MAXIMUM_NO_OUTLIERS = "maximum without outliers"
    MINIMUM_NO_OUTLIERS = "minimum without outliers"
    BINS = "histogram frequencies"


    # why are we using percentiles 05 and 95 instead of the more common
    # 2nd, 9th, 91st and 98th percentiles? Or even the 10th and 90th
    # percentiles (as in Bowley's seven figure summary)?
    # https://en.wikipedia.org/wiki/Box_plot
    PARAMETER_CHOICES = (
        (MEAN, MEAN),
        (MEDIAN, MEDIAN),
        (MAXIMUM, MAXIMUM),
        (MINIMUM, MINIMUM),
        (TIMELINESS, TIMELINESS),
        (FIRST_QUARTILE, FIRST_QUARTILE),
        (THIRD_QUARTILE, THIRD_QUARTILE),
        (PERCENTILE_05, PERCENTILE_05),
        (PERCENTILE_95, PERCENTILE_95),
        (STANDARD_DEVIATION, STANDARD_DEVIATION),
        (FILE_SIZE, FILE_SIZE),
        (PROCESSED_PIXELS, PROCESSED_PIXELS),
        (POSITIVE_OUTLIERS, POSITIVE_OUTLIERS),
        (NEGATIVE_OUTLIERS, NEGATIVE_OUTLIERS),
        (MAXIMUM_NO_OUTLIERS, MAXIMUM_NO_OUTLIERS),
        (MINIMUM_NO_OUTLIERS, MINIMUM_NO_OUTLIERS),
        (BINS, BINS),
    )

    name = models.CharField(max_length=50, choices=PARAMETER_CHOICES)
    value = models.CharField(max_length=999999999)
    product = models.ForeignKey(ProductQualityMonitor,
                                related_name="parameters")

    def __unicode__(self):
        return "{}: {}".format(self.name, self.value)

