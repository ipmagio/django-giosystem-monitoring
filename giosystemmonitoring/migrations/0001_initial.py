# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Operation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ecflow_host', models.CharField(help_text=b'Name of the ecFlow server that ran this operation', max_length=50)),
                ('ecflow_path', models.CharField(help_text=b'ecFlow path to the task that ran this operation', max_length=255)),
                ('suite', models.CharField(help_text=b'ecFlow suite name', max_length=255)),
                ('task', models.CharField(help_text=b'ecFlow task name', max_length=255)),
                ('timestamp_begin', models.DateTimeField(help_text=b'Creation date for this operation')),
                ('timestamp_end', models.DateTimeField(help_text=b'End date for this operation', null=True, blank=True)),
                ('timeslot', models.DateTimeField(help_text=b'Timeslot for the package that the operation ran')),
                ('status', models.CharField(default=b'RUNNING', max_length=20, choices=[(b'WAITING', b'WAITING'), (b'RUNNING', b'RUNNING'), (b'FINISHED', b'FINISHED')])),
                ('result', models.BooleanField(default=False)),
                ('observation', models.TextField(help_text=b"Additional details about the operation's result")),
                ('host', models.CharField(help_text=b'Name of the host where this operation ran', max_length=50)),
                ('settings_uri', models.CharField(help_text=b'URI of the settings that were used when configuring the package', max_length=255)),
                ('package', models.CharField(help_text=b'Name of the package that this operation ran', max_length=50)),
                ('number_of_executions', models.SmallIntegerField(default=0, help_text=b'How many times has this task been run. ')),
            ],
        ),
        migrations.CreateModel(
            name='ProductQualityMonitor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_name', models.CharField(help_text=b'Name of the product that is being monitored', max_length=50, choices=[(b'LST', b'LST'), (b'LST10', b'LST10')])),
                ('area', models.CharField(default=b'Globe', max_length=10, choices=[(b'Globe', b'Globe'), (b'Euro', b'Euro'), (b'Afri', b'Afri'), (b'Asia', b'Asia'), (b'Noam', b'Noam'), (b'Soam', b'Soam'), (b'Ocea', b'Ocea')])),
                ('timeslot', models.DateTimeField(help_text=b'Product timeslot')),
                ('processing_line', models.CharField(max_length=50, blank=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='QualityParameter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, choices=[(b'mean', b'mean'), (b'median', b'median'), (b'maximum', b'maximum'), (b'minimum', b'minimum'), (b'timeliness', b'timeliness'), (b'first quartile', b'first quartile'), (b'third quartile', b'third quartile'), (b'percentile 5', b'percentile 5'), (b'percentile 95', b'percentile 95'), (b'standard deviation', b'standard deviation'), (b'file size', b'file size'), (b'processed pixels', b'processed pixels'), (b'positive outliers', b'positive outliers'), (b'negative outliers', b'negative outliers'), (b'maximum without outliers', b'maximum without outliers'), (b'minimum without outliers', b'minimum without outliers'), (b'histogram frequencies', b'histogram frequencies')])),
                ('value', models.CharField(max_length=999999999)),
                ('product', models.ForeignKey(related_name='parameters', to='giosystemmonitoring.ProductQualityMonitor')),
            ],
        ),
    ]
