from __future__ import absolute_import
import datetime

from rest_framework import generics, filters
from rest_framework.views import APIView
from rest_framework.response import Response
import django_filters
import numpy as np

from . import serializers
from .models import ProductQualityMonitor, QualityParameter, Operation


class OperationFilters(django_filters.FilterSet):
    suite = django_filters.CharFilter(name='suite')
    class Meta:
        model = Operation
        fields = {
            'suite': ['exact', 'iexact'],
            'timeslot': ['exact', 'year', 'month', 'day', 'regex'],
            'ecflow_path': ['exact', 'startswith', 'endswith'],
            'ecflow_host': ['exact'],
            'status': ['exact', 'iexact'],
            'result': ['exact', 'iexact'],
        }

class OperationList(generics.ListCreateAPIView):
    queryset = Operation.objects.all()
    serializer_class = serializers.OperationSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = OperationFilters


class OperationDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Operation.objects.all()
    serializer_class = serializers.OperationSerializer


class ProductQualityMonitorFilters(django_filters.FilterSet):
    timeslotRange = django_filters.DateFromToRangeFilter(name='timeslot')

    class Meta:
        model = ProductQualityMonitor
        fields = {'product_name': ['exact'],
                  'area': ['exact'],
                  'timeslot': ['timeslotRange','exact'],
                  }
        order_by = ['timeslot']

        
class ProductQualityMonitorList(generics.ListCreateAPIView):
    queryset = ProductQualityMonitor.objects.all()
    serializer_class = serializers.ProductQualityMonitorSerializer
    filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    filter_class = ProductQualityMonitorFilters
    search_fields = ('^area')


class ProductQualityMonitorDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductQualityMonitor.objects.all()
    serializer_class = serializers.ProductQualityMonitorSerializer


class QualityParameterList(generics.ListCreateAPIView):
    queryset = QualityParameter.objects.all()
    serializer_class = serializers.QualityParameterSerializer


class QualityParameterDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = QualityParameter.objects.all()
    serializer_class = serializers.QualityParameterSerializer


class AggregateStats(APIView):

    def get(self,request):

        # Query Parameters
        inicial_year = request.query_params.get('inicialYear')
        inicial_month = request.query_params.get('inicialMonth')
        inicial_day = request.query_params.get('inicialDay')
        final_year = request.query_params.get('finalYear')
        final_month = request.query_params.get('finalMonth')
        final_day = request.query_params.get('finalDay')
        area = request.query_params.get('area')
        frequency = int(request.query_params.get('frequency'))

        begin = datetime.datetime(int(inicial_year),
                                  int(inicial_month),
                                  int(inicial_day))
        end = datetime.datetime(int(final_year),
                                int(final_month),
                                int(final_day),
                                23, 59)

        parameters = ["mean", "maximum", "minimum", "timeliness",
                      "processed pixels", "positive outliers",
                      "negative outliers", "maximum without outliers",
                      "minimum without outliers", "histogram frequencies"]

        results = {'results': []}

        begin_date = begin

        nday = 0

        # calculating stats for the accumulated data
        while begin_date < end:

            nday += 1

            middle_date = begin + datetime.timedelta(
                days=(frequency * nday)) - datetime.timedelta(hours=1)

            stats = []
            
            for param in parameters:
                
                val = QualityParameter.objects.filter(
                    name=param, product__area=area,
                    product__timeslot__range=(begin_date, middle_date)
                )
                vals = []
                
                if param != 'histogram frequencies':
                
                    for item in val:
                        temp = float(item.value)
                        vals.append(temp)
                        
                    stats.append(vals)
                    
                else:

                    hist = []
                    
                    for item in val:
                        temp = [float(x) for x in item.value.split(',')]
                        hist.append(temp)
                        
                    result = [sum(k) for k in zip(*hist)]
                    agreg_vals = np.cumsum(result)
                    total = sum(result)
                    percentils = []
                    perc_numb = [5.0, 25.0, 50.0, 75.0, 95.0]

                    for numb in perc_numb:
                        rank = numb / 100 * (total + 1)
                        percentils.append(rank)

                    for perc in percentils:
                        for i, j in enumerate(np.arange(-100, 100, 1)):
                            if agreg_vals[i] < perc <= agreg_vals[i + 1]:
                                interp = ((j + 1) + (j + 2)) / 2.0
                                vals.append(interp)

                    stats.append(vals)

            aggregated_data = {
                'timeslot': begin_date,
                'area': area,
                'parameters': [
                    {'name': 'median', 'value': stats[9][2]},
                    {'name': 'mean', 'value': np.mean(stats[0])},
                    {'name': 'timeliness', 'value': np.mean(stats[3])},
                    {'name': 'first quartile', 'value': stats[9][1]},
                    {'name': 'third quartile', 'value': stats[9][3]},
                    {'name': 'percentile 5', 'value': stats[9][0]},
                    {'name': 'percentile 95', 'value': stats[9][4]},
                    {'name': 'standart deviation', 'value': 0},
                    {'name': 'maximum', 'value': max(stats[1])},
                    {'name': 'minimum', 'value': min(stats[2])},
                    {'name': 'positive outliers', 'value': np.mean(stats[5])},
                    {'name': 'negative outliers', 'value': np.mean(stats[6])},
                    {
                        'name': 'maximum without outliers',
                        'value': max(stats[7])
                    },
                    {
                        'name': 'minimum without outliers',
                        'value': min(stats[8])
                    },
                    {'name': 'processed pixels', 'value': np.mean(stats[4])}
                ]
            }

            results['results'].append(aggregated_data)
            
            begin_date = middle_date + datetime.timedelta(hours=1)
                      
        return Response(results)

class SummaryReport(APIView):

    def get(self,request):

        inicial_year = request.query_params.get('inicialYear')
        inicial_month = request.query_params.get('inicialMonth')
        inicial_day = request.query_params.get('inicialDay')
        final_year = request.query_params.get('finalYear')
        final_month = request.query_params.get('finalMonth')
        final_day = request.query_params.get('finalDay')

        begin = datetime.datetime(int(inicial_year),
                                  int(inicial_month),
                                  int(inicial_day))
        end = datetime.datetime(int(final_year),
                                int(final_month),
                                int(final_day),23,00)

        dif = (end-begin).days
        if dif == 0:
            prod_total = 24
        else:
            prod_total = (dif+1) * 24 

        gen_numb = (ProductQualityMonitor.objects.filter(
                    timeslot__range=(begin, end)
                ).count())/6

        if (gen_numb != 0):
            
            gen_perc = gen_numb * 100 / prod_total

            time = QualityParameter.objects.filter(
                    name="timeliness",
                    product__timeslot__range=(begin, end)
                )

            times = []
            vals = []

            for item in time:
                temp1 = float(item.value)
                vals.append(temp1)
                if temp1 <=24:
                    times.append(temp1)

            times_numb = len(times)/6

            times_perc = times_numb * 100 / prod_total

            times_mean = np.mean(vals)
            times_max = max(vals)

            vol = QualityParameter.objects.filter(
                    name="file size",
                    product__timeslot__range=(begin, end)
                )

            volumes = []
            
            for val in vol:
                temp2 = float(val.value)
                volumes.append(temp2)

            total_volume = round(sum(volumes)*(10e-6)/6,3)

            ajax_results = {'records': [], "queryRecordCount": 7, "totalRecordCount": 7}

            ajax_results['records'].append({"description":"Percentage of generated products/files [%]",
                          "lst": gen_perc})
            ajax_results['records'].append({"description":"Number of generated products/files",
                          "lst": gen_numb})
            ajax_results['records'].append({"description":"Percentage of generated products/files within timeliness [%]",
                          "lst": times_perc})
            ajax_results['records'].append({"description":"Number of generated products/files within timeliness",
                          "lst": times_numb})
            ajax_results['records'].append({"description":"Average of product timeliness [Hours]",
                         "lst": times_mean})
            ajax_results['records'].append({"description":"Maximum of product timeliness [Hours]",
                         "lst": times_max})
            ajax_results['records'].append({"description":"Volume of archived products [Mb]",
                         "lst": total_volume})

        else:
            ajax_results = {'records': [], "queryRecordCount": 7, "totalRecordCount": 7}
        
        return Response(ajax_results)
