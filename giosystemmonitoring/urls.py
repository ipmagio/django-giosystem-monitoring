from django.conf.urls import url
from giosystemmonitoring import views

urlpatterns = [
    url(r'^operation/$', views.OperationList.as_view()),
    url(r'^operation/(?P<pk>[0-9]+)$', views.OperationDetail.as_view()),
    url(r'^productqualitymonitor/$', views.ProductQualityMonitorList.as_view()),
    url(r'^productqualitymonitor/(?P<pk>[0-9]+)$', views.ProductQualityMonitorDetail.as_view()),
    url(r'^qualityparameter/$', views.QualityParameterList.as_view()),
    url(r'^qualityparameter/(?P<pk>[0-9]+)$', views.QualityParameterDetail.as_view()),
    url(r'^aggregatedstats/$', views.AggregateStats.as_view()),
    url(r'^summaryreport/$', views.SummaryReport.as_view()),
]


