from django.contrib import admin
import models


@admin.register(models.Operation)
class OperationAdmin(admin.ModelAdmin):
    date_hierarchy = "timeslot"
    list_filter = ("result", "host", "status", "suite")
    search_fields = ["ecflow_path", "suite", "task"]
    list_display = ("ecflow_path", "package", "host", "timeslot", "result",
                    "status", "execution_time")


class ParameterInline(admin.StackedInline):
    model = models.QualityParameter
    extra = 1


@admin.register(models.ProductQualityMonitor)
class ProductQualityMonitorAdmin(admin.ModelAdmin):
    date_hierarchy = "timeslot"
    list_filter = ("product_name", "area", "processing_line",)
    list_display = ("id", "product_name", "area", "timeslot",
                    "processing_line", "get_measured_parameters",)
    inlines = [ParameterInline]
